<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'zbawiciel' );

/** MySQL database username */
define( 'DB_USER', '' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'Qad9EvQYDyFvaSScEd+KcFaEv3DRtFnyeHnYuZ9OTermhBuCDSeR6ulDgUxbLgUWCYijTVqoyiuw3PAqjGz0NQ==');
define('SECURE_AUTH_KEY',  '5/tOy/eS1mcnos/sNODoaEhsn6F8X4quEygNWp9zbtb3oKaFPFEqyLwpSuUddH0pdKaRI+W15W3glVsCQoLH5A==');
define('LOGGED_IN_KEY',    'hWFtSc+FJs+EL0GpqQiFjT0pYdBPeWD1snNBmw426MPxKRb2lRfb9ZvL+JbmjZfF7Lvr12XLah6VS+Qcsp2Qsg==');
define('NONCE_KEY',        'DVgJDQstQHDHAIgMHFH91XsL3ZTnEmx20eK9tx8urUZr7Xde3FfRhSKPiz0By0zYQPbNuk3Swy8cHjfhZkINyQ==');
define('AUTH_SALT',        '5vVGpJoMu8Osd5FeBfAmGBh+rjBj59+++sm82/WzZexNsMIVP5F/f5mpGD/r4Cd8TZKUrfrW9BzGmby4vz8uuQ==');
define('SECURE_AUTH_SALT', 'IqkdTsKqrqY+1a46rsTF/lJaY8gRHmU5+yRb59O/r+lWX49iuPZPwwb56AJixrDagstY1cWT4Lw/kwZ5uIesUA==');
define('LOGGED_IN_SALT',   '8Xfdv8J1sRT4u579lduhCs0/0BMuXXYttVCsKkJE3eIHvqPoJqXLiN/Ir3GageShzGbTRm70pQKpSbO64HO4sQ==');
define('NONCE_SALT',       '4CXHpVF3nsdV0Pa2vgFyNVWiT/kSmD7466qQSXDJF16JGyxN/to1ktqgrJFodqLSTMbAymQQW0rlfIv5hnD+DA==');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';


/* Inserted by Local by Flywheel. See: http://codex.wordpress.org/Administration_Over_SSL#Using_a_Reverse_Proxy */
if (isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https') {
	$_SERVER['HTTPS'] = 'on';
}
/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) )
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
